class EmployeeOne {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}


const employeeOne = new EmployeeOne("Іван", 30, 50000);
console.log(`Ім'я: ${employeeOne.name}, Вік: ${employeeOne.age}, Зарплата: ${employeeOne.salary}`);


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }


    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }


    set name(newName) {
        this._name = newName;
    }

    set age(newAge) {
        this._age = newAge;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}


const employee1 = new Employee("Іван", 30, 50000);


console.log(`Ім'я: ${employee1.name}, Вік: ${employee1.age}, Зарплата: ${employee1.salary}`);


employee1.name = "Петро";
employee1.age = 35;
employee1.salary = 60000;


console.log(`Оновлені дані: Ім'я: ${employee1.name}, Вік: ${employee1.age}, Зарплата: ${employee1.salary}`);



class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }


    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}


const programmer1 = new Programmer("Іван", 30, 70000, ["JavaScript", "Python"]);


console.log(`Ім'я: ${programmer1.name}, Вік: ${programmer1.age}, Зарплата: ${programmer1.salary}`);
console.log(`Вивчає мови: ${programmer1.lang.join(', ')}`);


programmer1.lang = ["Java", "C#"];


console.log(`Оновлені дані: Ім'я: ${programmer1.name}, Вік: ${programmer1.age}, Зарплата: ${programmer1.salary}`);
console.log(`Вивчає мови: ${programmer1.lang.join(', ')}`);



class Programmer1 extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(newSalary) {
        // Додай сеттер для salary для забезпечення правильної поведінки
        this._salary = newSalary / 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

// Приклад використання:
const programmer = new Programmer1("Іван", 30, 70000, ["JavaScript", "Python"]);

console.log(`Оновлена зарплата: ${programmer.salary}`);




const programmerOne = new Programmer("Іван", 30, 70000, ["JavaScript", "Python"]);
const programmerTwo = new Programmer("Марія", 28, 75000, ["Java", "C++"]);
const programmerThree = new Programmer("Олексій", 35, 80000, ["Ruby", "Swift"]);


console.log("Програмісти:");
console.log(`1. Ім'я: ${programmerOne.name}, Зарплата: ${programmerOne.salary}, Мови: ${programmerOne.lang.join(', ')}`);
console.log(`2. Ім'я: ${programmerTwo.name}, Зарплата: ${programmerTwo.salary}, Мови: ${programmerTwo.lang.join(', ')}`);
console.log(`3. Ім'я: ${programmerThree.name}, Зарплата: ${programmerThree.salary}, Мови: ${programmerThree.lang.join(', ')}`);
